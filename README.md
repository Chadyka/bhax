# BHAX

**_A repository for excercises during the course of High Level Programming 1._**

## If you are looking for the current state of my textbook for "labor" excersises, go to:

> Chadyka/bhax/lab-textbook/bhax-textbook-fdl.pdf

## Find my excercise folders in:

> Chadyka/bhax/projects/excercises/{Chapter_name}
