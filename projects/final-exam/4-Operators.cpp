#include <iostream>

namespace Prog1 {
    class Foo {};

    void bar(Foo foo) {
        std::cout << "hello" << std::endl;
    }
}

int main()
{
    std::cout << "Test\n"; // There is no operator<< in global namespace, but Argument-dependent lookup (Koenig lookup)
                           // examines std namespace because the left argument is in
                           // std and finds std::operator<<(std::ostream&, const char*)

    operator<<(std::cout, "Test\n"); // same, using function call notation

    Prog1::Foo foo;
    Prog1::bar(foo);
    bar(foo);

    return 0;
}