A nyelvtan és az általa generált nyelv definíciója szerint minden nyelvtanhoz egy egyértelműen meghatározott nyelv tartozik, de megfordítva, egy nyelvet nem csak egy nyelvtannal generálhatunk.
Chomsky-féle nyelvtanoknak több fajtája van:  mondatszerkezetű nyelvtan, szóhossznemcsökkentő, vagy monoton nyelvtan, környezetfüggő nyelvtan, környezetfüggetlen nyelvtan, lineáris nyelvtan és reguláris nyelvtan.
Minden generatív nyelv tartalmaz kezdőszimbólumokat, terminális szimbólumokat avagy betűket, nemterminális szimbólumokat és helyettesítési szabályokat.

Egy környezetfüggő nyelv esetén mindig tudunk olyan algoritmust írni amely képes eldönteni, hogy az adott szó része-e monoton nyelvtan által generált nyelvnek.



F, G, H (változók)
x, y, z (konstansok)

Nyelv helyettesítési szabályai:
F -> xFyz, Fy -> Gzz, xG -> H, Hz -> xxyy

(addig hajtunk végre helyettesítést, amíg a szavunk csak konstansokból fog állni)

F a kezdőszimbólum:
F (F -> xFyz)
xFyz (Fy -> Gzz)
xGzzz (xG -> H)
Hzzz (Hz -> xxyy)
xxyyzz


F, G, H (változók)
x, y, z (konstansok)

Nyelv helyettesítési szabályai:
F -> zGH, zG -> xFx, xH -> yz, xF -> xxHy, Hy -> xz

(addig hajtunk végre helyettesítést, amíg a szavunk csak konstansokból fog állni)

F a kezdőszimbólum:
F (F -> zGH)
zGH (zG -> xFx)
xFxH (xH -> yz)
xFyz (xF -> xxHy)
xxHyyz (Hy -> xz)
xxxzyyz