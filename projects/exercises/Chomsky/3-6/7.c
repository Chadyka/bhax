#include <stdio.h>

int f(int x)
{
    return x * 10;
}

int main()
{
    int a = 50;
    printf("%d %d", f(a), a);
    return 0;
}
