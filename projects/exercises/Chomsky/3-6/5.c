#include <stdio.h>
int main()
{
    int i, n = 100, *d = &n, *s = &n;
    for (i = 0; i < n && (*d++ = *s++); ++i)
    {
        printf("i = %d; n = %d; *d = %p; *s = %p;\n", i, n, d, s);
    }
    return 0;
}
