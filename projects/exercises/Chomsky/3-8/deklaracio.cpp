#include <stdio.h>
#include <stdlib.h>

typedef int (*F)(int, int);
typedef int (*(*G)(int))(int, int);

int *g(void) // Egeszre mutato mutatot visszaado fuggveny
{
    int q = 20;
    int *p = &q;
    return p;
}

int sum(int a, int b)
{
    return a + b;
}

int multiply(int a, int b)
{
    return a * b;
}

F sumormul(int c)
{
    if (c)
    {
        return multiply;
    }
    else
    {
        sum;
    }
}

int main(int argc, char const *argv[])
{
    int a = 4;   // Egész
    int *b = &a; // Egeszre mutato mutato
    printf("a * (*b) = %d\n", multiply(a, *b));

    int &r = a; // Egesz referenciaja
    r = 6;
    printf("a * (*b) = %d (referenciával változtatás után)\n", multiply(a, *b));

    int tomb[5]; // Egeszek tombje

    printf("tomb = [ ");
    for (int i = 0; i < 5; i++)
    {
        tomb[i] = i;
        printf("%d ", tomb[i]);
    }
    printf("]\n");

    int(&d)[5] = tomb; // Egeszek tombjenek referenciaja

    printf("tomb = [ ");
    for (int i = 0; i < 5; i++)
    {
        d[i] = i + 2;
        printf("%d ", tomb[i]);
    }
    printf("] (referenciával változtatás után)\n");

    int *e[5]; // Egeszre mutato mutatok tombje

    printf("*e = [ ");
    for (int i = 0; i < 5; i++)
    {
        e[i] = &tomb[i];
        printf("%d ", *e[i]);
    }
    printf("]\n");

    printf("visszaadott pointer = %p\n", g()); // Egeszre mutato mutatot visszaado fuggveny

    F f = sum; //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény

    printf("%d\n", f(2, 3));

    G g = sumormul; // függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre

    f = *g(42);

    printf("%d\n", f(2, 3));

    return 0;
}
