#ifndef ANTTHREAD_H
#define ANTTHREAD_H

#include <QThread>
#include "ant.h"

// QThread osztályból származtatjuk az osztályt,
// hogy tudjuk használni a Qt keretrendszer függvényeit
class AntThread : public QThread
{
    Q_OBJECT

public:
    AntThread(Ants * ants, int ***grids, int width, int height,
             int delay, int numAnts, int pheromone, int nbrPheromone,
             int evaporation, int min, int max, int cellAntMax);

    ~AntThread();

    void run();

    // a program állapotát változtatja
    // run() fv-n belül a while fejében negálja a true értéket
    void finish()
    {
        running = false;
    }

    // a program megállításáért felelős
    void pause()
    {
        paused = !paused;
    }

    // visszaadja a program állapotát
    bool isRunnung()
    {
        return running;
    }

private:
    bool running {true};
    bool paused {false};
    Ants* ants;
    int** numAntsinCells;
    int min, max;
    int cellAntMax;
    int pheromone;
    int evaporation;
    int nbrPheromone;
    int ***grids;
    int width;
    int height;
    int gridIdx;
    int delay;

    void timeDevel();

    int newDir(int sor, int oszlop, int vsor, int voszlop);
    void detDirs(int irany, int& ifrom, int& ito, int& jfrom, int& jto );
    int moveAnts(int **grid, int row, int col, int& retrow, int& retcol, int);
    double sumNbhs(int **grid, int row, int col, int);
    void setPheromone(int **grid, int row, int col);

signals:
    // signal-ként adjuk meg a step() fv-t így slot-ot tudunk hozzá kötni
    void step ( const int &);

};

#endif
