#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
    WINDOW *window;
    window = initscr();

    int x = 0;
    int y = 0;

    int Xmotion = 0;
    int Ymotion = 0;

    int Xmax;
    int Ymax;

    // getmaxyx() függvény megadja az ablak max szélességét és magasságát Ymax és Xmax változókba
    // szétbontva lehetne Ymax = getmaxy(ablak); Xmax = getmaxx(ablak);
    getmaxyx(window, Ymax, Xmax);

    // a késsőbiekben osztjuk 2-vel a koordinátáinkat, hogy egyesével lépjenek és ne kettessével,
    // így az Xmax és Ymax változóinkat beszorozzuk kettővel hogy ne csak az ablak felében "pattogjunk"
    Xmax *= 2;
    Ymax *= 2;

    for (;;)
    {
        // a modulo használata miatt mindegy, hogy x nagyobb mint xmax, 
        // mert pl.: x = 22 és xmax = 120 is 22-t ad ugyanúgy mint x = 142 esetén
        x = (x - 1) % Xmax;
        Xmotion = (Xmotion + 1) % Xmax;

        y = (y - 1) % Ymax;
        Ymotion = (Ymotion + 1) % Ymax;

        // y és x koordinátái, az abs() függvény tartja a kurzort a képernyőn, így nem tudunk stdsrc-n kívül menni
        //
        mvprintw(abs((y + (Ymax - Ymotion))/2), abs((x + (Xmax - Xmotion))/2), "");

        // az ablakra való kiírás nem jelenik meg amíg nem frissítjük a stdsrc-t
        // ebben az esetben wrefresh(stdscr) az alapértelmezett ami ekvivalens a refresh() függvénnyel
        refresh();
        
        // a program álljon le a usleep() paraméterének megadott microszekundumig
        usleep(50000);
    }
    return 0;
}
