#include <stdio.h>

int main()
{
    int a, b, tmp;

    printf("Enter the first number: ");
    scanf("%d",&a);
    
    printf("Enter the second number: ");
    scanf("%d",&b);

    tmp = a;
    a = b;
    b = tmp;

    printf("The numbers in reverse order: %d --- %d\n",a,b);

    return 0;
}