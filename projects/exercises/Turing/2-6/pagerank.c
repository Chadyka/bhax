#include <stdio.h>
#include <math.h>

// egyszerű függvény ami kiírja a kapott double tömb int darab elemét
void kiir(double arr[], int db)
{
    for (int i = 0; i < db; i++)
    {
        printf("%lf\n", arr[i]);
    }
}

// kiszámolja az PR és PRv tömbök számtani távolságát (ha elég kicsi a távolság a programunk eléri a végeredményét)
double tavolsag(double PR[], double PRv[], int n)
{
    double sum = 0.0;
    for (int i = 0; i < n; ++i)
    {
        sum += (PRv[i] - PR[i]) * (PRv[i] - PR[i]);
    }

    return sqrt(sum);
}

int main()
{
    // inicializálunk egy 4x4-es tömböt ami a 4 oldalunk linkjeinek értékét tárolja
    double L[4][4] = {
        {0.0, 0.0, 1.0 / 3.0, 0.0},
        {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
        {0.0, 1.0 / 2.0, 0.0, 0.0},
        {0.0, 0.0, 1.0 / 3.0, 0.0}};

    // két másik double tömböt is inicializálunk, az elsőben az oldalaink értékét tároljuk majd,
    // a másodikban pedig 1/4 értékeket amik az oldalak kezdőértékei értékei lesznek
    double PR[4] = {0.0, 0.0, 0.0, 0.0};
    double PRv[4] = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};

    int i, j;

    for (;;)
    {
        // végig lépkedünk a tömbökön
        for (int i = 0; i < 4; i++)
        {
            PR[i] = 0.0;
            for (int j = 0; j < 4; j++)
            {
                // Megadjuk egy oldalunk értékét a linkmátrix és a linkek értékének segítségével
                PR[i] += (L[i][j] * PRv[j]);
            }
        }

        // Amennyiben a tavolsag() függvény 0.00001-nél kisebb értékkel tér vissza kilépünk a ciklusból
        if (tavolsag(PR, PRv, 4) < 0.00001)
        {
            break;
        }

        // Az oldalak értékei változtak, így a PRv segéddtömb megkapja az oldalak jelenlegi értékeit
        for (int i = 0; i < 4; i++)
        {
            PRv[i] = PR[i];
        }
    }

    // kiíratjuk az oldalak kiszámolt értékeit
    kiir(PR, 4);

    return 0;
}