#include "frakablak.h"

// KONSTRUKTOR
// Inicializálja a Qt ablakunkat és az első iterációját a mandelbrot halmaznak
FrakAblak::FrakAblak(double a, double b, double c, double d,
                     int szelesseg, int iteraciosHatar, QWidget *parent)
    : QMainWindow(parent)
{
    // setWindowTitle() függvény beszédes nevű,
    // beállítja az ablak megjelenítendő nevét
    setWindowTitle("Mandelbrot halmaz");

    // számítást kezdünk tehát az ezt jelölő bool = true
    szamitasFut = true;
    // alaphelyzetbe állítjuk a nagyító téglalapjához
    // használt változókat
    x = y = mx = my = 0;
    // a mandelbrot halmaz számolásához
    // szükséges adatok beállítása
    this->a = a;
    this->b = b;
    this->c = c;
    this->d = d;
    this->szelesseg = szelesseg;
    this->iteraciosHatar = iteraciosHatar;
    magassag = (int)(szelesseg * ((d - c) / (b - a)));

    // Megadja az ablak magasságát és szélességét
    setFixedSize(QSize(szelesseg, magassag));
    // inicializál egy Qt kép objektumot ami az ablakban megjelenő "vászon" lesz
    fraktal = new QImage(szelesseg, magassag, QImage::Format_RGB32);

    // Kirajzoljuk a halmazt
    mandelbrot = new FrakSzal(a, b, c, d, szelesseg, magassag, iteraciosHatar, this);
    mandelbrot->start();
}

// A frakablak dekonstruktora
FrakAblak::~FrakAblak()
{
    delete fraktal;
    delete mandelbrot;
}

// A nagyítót megvalósító függvény
void FrakAblak::paintEvent(QPaintEvent *)
{
    // példányosítunk egy qpainter objektumot
    QPainter qpainter(this);
    qpainter.drawImage(0, 0, *fraktal);
    if (!szamitasFut)
    {
        // Fehér 1px vastagságú rajzoló eszközt használunk
        qpainter.setPen(QPen(Qt::white, 1));
        // téglalapot jelenítünk meg a x, y, mx, my paraméterekből
        // ahol az x, y a bal felső sarka a téglalapnak, az mx az x tengellyel
        // párhuzamos oldalának hossza az my pedig az y tengellyel párhuzamos
        // oldal hossza
        qpainter.drawRect(x, y, mx, my);
    }
    qpainter.end();
}

// Az egér lenyomásakor lefutó függvény
void FrakAblak::mousePressEvent(QMouseEvent *event)
{

    // A nagyítandó kijelölt területet bal felső sarka:
    x = event->x();
    y = event->y();
    mx = 0;
    my = 0;

    // frissíti a megjelenő ablakot
    update();
}

// Az egér mozgatásakor lefutó függvény
void FrakAblak::mouseMoveEvent(QMouseEvent *event)
{

    // A nagyítandó kijelölt terület szélessége és magassága:
    mx = event->x() - x;
    my = mx; // négyzet alakra hozás

    // frissíti a megjelenő ablakot
    update();
}

// Az egér elengedésekor lefutó függvény
void FrakAblak::mouseReleaseEvent(QMouseEvent *event)
{
    // ha már fut a számítás kilép
    if (szamitasFut)
        return;

    // számítást kezdünk tehát az ezt jelölő bool = true
    szamitasFut = true;

    // A számításhoz szükséges adatok kiszámolása
    double dx = (b - a) / szelesseg;
    double dy = (d - c) / magassag;

    double a = this->a + x * dx;
    double b = this->a + x * dx + mx * dx;
    double c = this->d - y * dy - my * dy;
    double d = this->d - y * dy;

    this->a = a;
    this->b = b;
    this->c = c;
    this->d = d;

    // Töröljük a nagyító előző állapotát
    delete mandelbrot;

    // megrajzoljuk a nagyított részt előről
    mandelbrot = new FrakSzal(a, b, c, d, szelesseg, magassag, iteraciosHatar, this);
    mandelbrot->start();

    // frissíti a megjelenő ablakot
    update();
}

// Billentyű lenyomásakor
void FrakAblak::keyPressEvent(QKeyEvent *event)
{
    // ha már fut a számítás kilép
    if (szamitasFut)
        return;

    // N billentyű lenyomására az iterációs határt szorozzuk 2-vel
    if (event->key() == Qt::Key_N)
        iteraciosHatar *= 2;

    // számítást kezdünk tehát az ezt jelölő bool = true
    szamitasFut = true;

    // Töröljük a nagyító előző állapotát
    delete mandelbrot;

    // megrajzoljuk az új halmazt előről
    mandelbrot = new FrakSzal(a, b, c, d, szelesseg, magassag, iteraciosHatar, this);
    mandelbrot->start();
}

// megkapja az ablak egy sorát amit a lenti módon kiszínez
void FrakAblak::vissza(int magassag, int *sor, int meret)
{
    for (int i = 0; i < meret; ++i)
    {
        // szín objektumot példányosítunk ahol a szín rgb(170, 255, 0)-tól
        // indul és a zöld értéke változik iterációnként
        QRgb szin = qRgb(170, 255 - sor[i], 0);
        // Kiszínezzük a sor adott pixelét a feljebb deklarált színre
        fraktal->setPixel(i, magassag, szin);
    }

    // frissíti a megjelenő ablakot
    update();
}

// "lenullázza"  a nagyítást és a változókat "alaphelyzetbe" állítja
void FrakAblak::vissza(void)
{
    szamitasFut = false;
    x = y = mx = my = 0;
}
