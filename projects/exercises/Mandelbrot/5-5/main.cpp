#include <QApplication>
#include "frakablak.h"

int main(int argc, char *argv[])
{
   // példányosítunk egy Qt applikációt ami
   //átveszi a parancssori argumentumokat
   QApplication a(argc, argv);

   // Pédányosítunk egy FrakAblak-ot ami a Qt ablakunk lesz
   FrakAblak w1;

   // show() függvény megnyitja a Qt ablakot
   w1.show();

   // visszatérésül futtatjuk a Qt alkalmazást
   return a.exec();
}
