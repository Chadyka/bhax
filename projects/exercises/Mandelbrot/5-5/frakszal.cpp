#include "frakszal.h"

// FrakSzal konstruktora
// Ebben a fájlban fog lefutni a mandelbrot halmaz rajzolása
FrakSzal::FrakSzal(double a, double b, double c, double d,
                   int szelesseg, int magassag, int iteraciosHatar, FrakAblak *frakAblak)
{
    this->a = a;
    this->b = b;
    this->c = c;
    this->d = d;
    this->szelesseg = szelesseg;
    this->iteraciosHatar = iteraciosHatar;
    this->frakAblak = frakAblak;
    this->magassag = magassag;

    egySor = new int[szelesseg];
}

// FrakSzal dekonstruktora
FrakSzal::~FrakSzal()
{
    delete[] egySor;
}

// A mandelbrot halmaz számítását végzi
// A belső for ciklus kiszámol egy sort majd
// átadja a frakablak.cpp-nek ami a színezést végzi
// majd a végén az üres vissza() fv. visszaállítja
// a nagyító alaphelyzetét
void FrakSzal::run()
{
    // A [a,b]x[c,d] tartományon milyen sűrű a
    // megadott szélesség, magasság hl:
    double dx = (b - a) / szelesseg;
    double dy = (d - c) / magassag;
    double reC, imC, reZ, imZ, ujreZ, ujimZ;
    // Hány iterációt csináltunk?
    int iteracio = 0;
    // Végigzongorázzuk a szélesség x magasságot:
    for (int j = 0; j < magassag; ++j)
    {
        //sor = j;
        for (int k = 0; k < szelesseg; ++k)
        {
            // c = (reC, imC) a háló rácspontjainak
            // megfelelő komplex szám
            reC = a + k * dx;
            imC = d - j * dy;
            // z_0 = 0 = (reZ, imZ)
            std::complex<double> c(reC, imC);

            reZ = 0;
            imZ = 0;
            std::complex<double> z_n(reZ, imZ);
            iteracio = 0;
            // z_{n+1} = z_n * z_n + c iter�ci�k
            // számítása, amíg |z_n| < 2 vagy míg
            // nem értük el a 255 iterációt, ha
            // viszont elértük, akkor úgy vesszük,
            // hogy a kiindulási c komplex számra
            // az iteráció konvergens, azaz a c a
            // Mandelbrot halmaz eleme
            while (std::abs(z_n) < 4 && iteracio < iteraciosHatar)
            {
                z_n = z_n * z_n + c;

                ++iteracio;
            }

            // ha a < 4 feltétel nem teljesült és a
            // iteráció < iterációsHatár lépett ki, azaz
            // feltesszük a c-ről, hogy itt a z_{n+1} = z_n * z_n + c
            // sorozat konvergens, azaz iteráció = iterációsHatár
            // ekkor az iteráció %= 256 egyenlő 255, mert az esetleges
            // nagyítasok során az iteráció = valahány * 256 + 255

            iteracio %= 256;

            // Kiszámolunk egy sort
            egySor[k] = iteracio;
        }
        // A kiszámolt sort a frakablak.cpp fogja számolni
        frakAblak->vissza(j, egySor, szelesseg);
    }
    // lenulláza a nagyító helyzetét
    frakAblak->vissza();
}
