#include <iostream>
#include "png++/png.hpp"
#include <complex>

int main(int argc, char *argv[])
{
    // megadjuk a számításhoz szükséges adatokat
    // értékkészlet, értelmezési tartomány,
    // szélső értékek és a kép méretei,
    // valamint a konstans c értékét
    int szelesseg = 1920;
    int magassag = 1080;
    int iteraciosHatar = 255;
    double xmin = -1.9;
    double xmax = 0.7;
    double ymin = -1.3;
    double ymax = 1.3;
    double reC = .285, imC = 0;
    double R = 10.0;

    // a futtatásnál megadott parancssori argumentumokat elmentjük változókba
    if (argc == 12)
    {
        // ezeket a szövegként megkapott értékeket int típusúra alakítjuk
        szelesseg = atoi(argv[2]);
        magassag = atoi(argv[3]);
        iteraciosHatar = atoi(argv[4]);
        // ezeket pedig lebegőpontos számokká
        xmin = atof(argv[5]);
        xmax = atof(argv[6]);
        ymin = atof(argv[7]);
        ymax = atof(argv[8]);
        reC = atof(argv[9]);
        imC = atof(argv[10]);
        R = atof(argv[11]);
    }
    // amennyiben nem kapott a progrm elég rgumentumot, irassuk ki a használati segítséget
    else
    {
        std::cout << "Hasznalat: ./biomorf fajlnev szelesseg magassag n a b c d reC imC R" << std::endl;
        return -1;
    }
    // létrehozunk egy kep objektumot ami az image
    // osztály példánya és rgb_pixel-ekből áll
    png::image<png::rgb_pixel> kep(szelesseg, magassag);

    // a képünk értékkészletét és értelmezési tartományát a képhez arányosítjuk
    double dx = (xmax - xmin) / szelesseg;
    double dy = (ymax - ymin) / magassag;

    // Júlia halmazoknál a Mandelbrot halmazokhoz képest
    // annyi a különbség, hogy itt a c nem változó hanem konstans
    // ezt a konstans c komplex számot mentjük a cc complex objektumba
    std::complex<double> cc(reC, imC);

    std::cout << "Szamitas\n";

    // j megy a sorokon
    for (int y = 0; y < magassag; ++y)
    {
        // k megy az oszlopokon

        for (int x = 0; x < szelesseg; ++x)
        {

            // c = (reC, imC) a halo racspontjainak
            // megfelelo komplex szam
            double reZ = xmin + x * dx;
            double imZ = ymax - y * dy;
            // complex objektumba tesszük a z komplex számunkat is
            std::complex<double> z_n(reZ, imZ);

            int iteracio = 0;
            // elvégezzük a számításokat a képlet szerint
            for (int i = 0; i < iteraciosHatar; ++i)
            {

                z_n = std::pow(z_n, 3) + cc;

                if (std::real(z_n) > R || std::imag(z_n) > R)
                {
                    iteracio = i;
                    break;
                }
            }

            // "megrajzoljuk" a képünket, piros, zöld és kék színek árnyalatait váltogatjuk
            // a maradékos osztás segítségével pixelenként
            // png::rgb_pixel() függvény mondja meg, hogy
            // az aktuális elem milyen színt fog kapni.
            // ebben az esetben kék színből keverük a legtöbbet
            kep.set_pixel(x, y,
                          png::rgb_pixel((iteracio * 10) % 255, (iteracio * 400) % 255, (iteracio * 80) % 255));
        }
        // kiírjuk, hogy a program éppen hány százalék elkészültségnél tart
        int szazalek = (double)y / (double)magassag * 100.0;
        std::cout << "\r" << szazalek << "%" << std::flush;
    }

    // a feltöltött kép objektumot az első argumentumban megkapott fájlba írjuk
    kep.write(argv[1]);
    std::cout << "\r" << argv[1] << " mentve." << std::endl;
}
