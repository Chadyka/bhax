#include <iostream>
#include "png++/png.hpp"
#include <complex>
// ./mandel mandel.png 1920 1080 1020 0.4127655418209589255340574709407519549131 0.4127655418245818053080142817634623497725 0.2135387051768746491386963270997512154281 0.2135387051804975289126531379224616102874

int main(int argc, char *argv[])
{
    // megadjuk a számításhoz szükséges adatokat
    // értékkészlet, értelmezési tartomány, szélső értékek és a kép méretei
    int szelesseg = 1920;
    int magassag = 1080;
    int iteraciosHatar = 255;
    double a = -1.9;
    double b = 0.7;
    double c = -1.3;
    double d = 1.3;

    // a futtatásnál megadott parancssori argumentumokat elmentjük változókba
    if (argc == 9)
    {
        // ezeket a szövegként megkapott értékeket int típusúra alakítjuk
        szelesseg = atoi(argv[2]);
        magassag = atoi(argv[3]);
        iteraciosHatar = atoi(argv[4]);
        // ezeket pedig lebegőpontos számokká
        a = atof(argv[5]);
        b = atof(argv[6]);
        c = atof(argv[7]);
        d = atof(argv[8]);
    }
    // amennyiben nem kapott a program elég argumentumot, irassuk ki a használati segítséget
    else
    {
        std::cout << "Hasznalat: ./3.1.2 fajlnev szelesseg magassag n a b c d" << std::endl;
        return -1;
    }

    // létrehozunk egy kep objektumot ami az image
    // osztály példánya és rgb_pixel-ekből áll
    png::image<png::rgb_pixel> kep(szelesseg, magassag);

    // a képünk értékkészletét és értelmezési tartományát a képhez arányosítjuk
    double dx = (b - a) / szelesseg;
    double dy = (d - c) / magassag;
    double reC, imC, reZ, imZ;
    int iteracio = 0;

    std::cout << "Szamitas\n";

    // j megy a sorokon
    for (int j = 0; j < magassag; ++j)
    {
        // k megy az oszlopokon

        for (int k = 0; k < szelesseg; ++k)
        {

            // c = (reC, imC) a halo racspontjainak
            // megfelelo komplex szam

            reC = a + k * dx;
            imC = d - j * dy;

            // itt már a compex library-t használjuk és a c változóban
            // tároljuk a komplex számunk kiszámított valós és képzetes részét
            std::complex<double> c(reC, imC);

            std::complex<double> z_n(0, 0);
            iteracio = 0;

            // a mandelbrot halmazt a képletével számoljuk
            while (std::abs(z_n) < 4 && iteracio < iteraciosHatar)
            {
                z_n = z_n * z_n + c;

                ++iteracio;
            }

            // "megrajzoljuk" a képünket, piros és zöld színek árnyalatait váltogatjuk
            // a maradékos osztás segítségével pixelenként
            kep.set_pixel(k, j,
                          png::rgb_pixel(iteracio % 255, (iteracio * iteracio) % 255, (iteracio * iteracio * iteracio) % 255));
        }

        // kiírjuk, hogy a program éppen hány százalék elkészültségnél tart
        int szazalek = (double)j / (double)magassag * 100.0;
        std::cout << "\r" << szazalek << "%" << std::flush;
    }

    // a feltöltött kép objektumot az első argumentumban megkapott fájlba írjuk
    kep.write(argv[1]);
    std::cout << "\r" << argv[1] << " mentve." << std::endl;
}
