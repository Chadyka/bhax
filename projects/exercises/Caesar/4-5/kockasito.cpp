// "Kockásító"
// Programozó Páternoszter
//
// Bátfai Norbert, nbatfai@inf.unideb.hu, nbatfai@gmail.com
// http://progpater.blog.hu/2011/03/26/kepes_egypercesek
//
// Fordítás:
// g++ kockapng.c++ `libpng-config --ldflags` -o kockapng

#include <iostream>
#include "png++/png.hpp"

void kocka(png::image<png::rgb_pixel> &kep, int r, int g, int b,
           int j, int i, int jjj, int iii)
{

    png::rgb_pixel rgb(r / iii * jjj, g / iii * jjj, b / iii * jjj);

    for (int jj = 0; jj < jjj; ++jj)
        for (int ii = 0; ii < iii; ++ii)
            kep.set_pixel(j + jj, i + ii, rgb);
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Hasznalat: ./kockapng bemeno_fajlnev kimeno_fajlnev";
        return -1;
    }

    // png-t készítünk a png++ csomaggal
    png::image<png::rgb_pixel> kep(argv[1]);

    std::cout << "Kockasitas";

    int ratlag = 0, gatlag = 0, batlag = 0, kocka_meret = 3, iii, jjj;

    for (int i = 0; i < kep.get_height(); i += kocka_meret)
    {
        for (int j = 0; j < kep.get_width(); j += kocka_meret)
        {

            ratlag = gatlag = batlag = 0;

            for (int ii = 0; ii < kocka_meret && i + ii < kep.get_height(); ++ii)
                for (int jj = 0; jj < kocka_meret && j + jj < kep.get_width(); ++jj)
                {

                    png::rgb_pixel rgb = kep.get_pixel(j + jj, i + ii);

                    ratlag += rgb.red;
                    gatlag += rgb.green;
                    batlag += rgb.blue;

                    iii = ii;
                    jjj = jj;
                }

            kocka(kep, ratlag, gatlag, batlag, j, i, jjj, iii);
        }
        std::cout << "." << std::flush;
    }

    kep.write(argv[2]);
    std::cout << argv[2] << " mentve" << std::endl;
}