#include <stdio.h>
#include <stdlib.h>

int main()
{
    // segédváltozó a pointer tömb méretéhez
    int nr = 5;
    // pointerek tömbje lesz
    double **tm;

    // lefoglalunk öt double*-nyi helyet a memóriában
    if ((tm = (double **)malloc(nr * sizeof(double *))) == NULL)
    {
        return -1;
    }

    // végigmegyünk az öt helyen és lefoglalunk helyenként egy double méretet
    for (int i = 0; i < nr; ++i)
    {
        // minden iterációval eggyel több double méretű memóriát foglalunk
        if ((tm[i] = (double *)malloc((i + 1) * sizeof(double))) == NULL)
        {
            return -1;
        }
    }

    // végigmegyünk a **tm pointer tömbünkön, így minden elemünknek értéket adva
    // fontos, hogy minden rákövetkező elem több elemű mint az előző
    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;

    // kiíratjuk az összes elemet amit eddig csináltunk
    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf("%f, ", tm[i][j]);
        printf("\n");
    }

    tm[3][0] = 42.0;         // dereferenciával értéket adunk a "tömb" 3. eleme első elemének
    (*(tm + 3))[1] = 43.0;   // mi van, ha itt hiányzik a külső () --> a [1] léptet egyet a dereferenciálás előtt  ezért az utolsó elem első eleme fog változni
    *(tm[3] + 2) = 44.0;     // harmadik elemben két pointerrel arrébb lépünk és értéket adunk
    *(*(tm + 3) + 3) = 45.0; // három poitert lépünk majd ezen a memóriaterületen is lépünk három pointernyit és értéket adunk

    printf("----------------------------\n");

    // kiírjuk a példákkal változtatott **tm-et
    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf("%f, ", tm[i][j]);
        printf("\n");
    }

    // felszabadítjuk az eddig lefoglalt memóriát
    for (int i = 0; i < nr; ++i)
        free(tm[i]);
    free(tm);

    return 0;
}
