#include "z3a18qa5_from_scratch.h"
#include <fstream>

class COVIR19Var
{
private:
	std::string loc;
	ZLWTree<char, '/', '0'> zlwbt;

public:
	COVIR19Var(std::string loc, std::string filename) : loc(loc)
	{
		std::ifstream ifs{filename};
		std::istreambuf_iterator<char> isbiEnd;

		for (std::istreambuf_iterator<char> isbi{ifs}; isbi != isbiEnd; ++isbi)
		{
			zlwbt << *isbi;
		}

		zlwbt.eval();
	}

	void print() { zlwbt.print(); }

	bool operator<(const COVIR19Var &other) const { return zlwbt < other.zlwbt; }

	double getMean() const { return zlwbt.getMean(); }
	double getVar() const { return zlwbt.getVar(); }
};

int main()
{
	COVIR19Var cv{"Severe acute respiratory syndrome coronavirus 2 isolate Wuhan-Hu-1, complete genome", "MN908947.3.01"};
	cv.print();

	std::cout << cv.getMean() << std::endl;
	std::cout << cv.getVar() << std::endl;

	return 0;
}
