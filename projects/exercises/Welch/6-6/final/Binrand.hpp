#include <random>
#include <functional>
#include <chrono>

class Unirand
{

private:
	std::function<int()> random;

public:
	Unirand(long seed, int min, int max) : random(
											   std::bind(
												   std::uniform_int_distribution<>(min, max),
												   std::default_random_engine(seed))) {}
	int operator()() { return random(); }
};

template <typename T>
class BinRandTree
{
protected:
	class Node
	{
	private:
		T value;
		Node *left;
		Node *right;
		int count{0};

		Node(const Node &);
		Node &operator=(const Node &);
		Node(Node &&);
		Node &operator=(Node &&);

	public:
		Node(T value) : value(value), left(nullptr), right(nullptr) {}
		T getValue() { return value; }
		Node *leftChild() { return left; }
		Node *rightChild() { return right; }
		void leftChild(Node *node) { left = node; }
		void rightChild(Node *node) { right = node; }
		int getCount() { return count; }
		void incCount() { ++count; }
	};

	Node *root;
	Node *treep;
	int depth{0};

private:
	// TODO rule of five
public:
	// Constructor
	BinRandTree(Node *root = nullptr, Node *treep = nullptr) : root(root), treep(treep)
	{
		std::cout << "BT ctor" << std::endl;
	}
	// Copy constructor
	BinRandTree(const BinRandTree &old)
	{
		std::cout << "BT copy ctor" << std::endl;

		root = cp(old.root, old.treep);
	}

	Node *cp(Node *node, Node *treep)
	{
		Node *newNode = nullptr;

		if (node)
		{
			newNode = new Node(node->getValue());

			newNode->leftChild(cp(node->leftChild(), treep));
			newNode->rightChild(cp(node->rightChild(), treep));

			if (node == treep)
			{
				this->treep = newNode;
			}
		}

		return newNode;
	}

	// Copy assignment
	BinRandTree &operator=(const BinRandTree &old)
	{
		std::cout << "BT copy assign" << std::endl;

		BinRandTree tmp{old};
		std::swap(*this, tmp);
		return *this;
	}

	// Move constructor
	BinRandTree(BinRandTree &&old)
	{
		std::cout << "BT move ctor" << std::endl;

		root = nullptr;
		*this = std::move(old);
	}

	// Move assignment
	BinRandTree &operator=(BinRandTree &&old)
	{
		std::cout << "BT move assign" << std::endl;

		std::swap(old.root, root);
		std::swap(old.treep, treep);

		return *this;
	}

	~BinRandTree()
	{
		std::cout << "BT destructor" << std::endl;
		delTree(root);
	}
	BinRandTree &operator<<(T value);
	void print() { print(root, std::cout); }
	void print(Node *node, std::ostream &os);
	void delTree(Node *node);

	Unirand ur{std::chrono::system_clock::now().time_since_epoch().count(), 0, 2};

	int whereToPut()
	{
		return ur();
	}
};


template <typename T>
BinRandTree<T> &BinRandTree<T>::operator<<(T value)
{

	int rnd = whereToPut();

	if (!treep)
	{

		root = treep = new Node(value);
	}
	else if (treep->getValue() == value)
	{

		treep->incCount();
	}
	else if (!rnd)
	{

		treep = root;
		*this << value;
	}
	else if (rnd == 1)
	{

		if (!treep->leftChild())
		{

			treep->leftChild(new Node(value));
		}
		else
		{

			treep = treep->leftChild();
			*this << value;
		}
	}
	else if (rnd == 2)
	{

		if (!treep->rightChild())
		{

			treep->rightChild(new Node(value));
		}
		else
		{

			treep = treep->rightChild();
			*this << value;
		}
	}

	return *this;
}

template <typename T>
void BinRandTree<T>::print(Node *node, std::ostream &os)
{
	if (node)
	{
		++depth;
		print(node->leftChild(), os);

		for (int i{0}; i < depth; ++i)
		{
			os << "---";
		}

		os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;

		print(node->rightChild(), os);
		--depth;
	}
}

template <typename T>
void BinRandTree<T>::delTree(Node *node)
{
	if (node)
	{
		delTree(node->leftChild());
		delTree(node->rightChild());

		delete node;
	}
}