#include "Binrand.hpp"

template <typename T, T vr, T v0>
class ZLWTree : public BinRandTree<T>
{

public:
	ZLWTree() : BinRandTree<T>(new typename BinRandTree<T>::Node(vr))
	{
		this->treep = this->root;
	}
	ZLWTree &operator<<(T value);
};

template <typename T, T vr, T v0>
ZLWTree<T, vr, v0> &ZLWTree<T, vr, v0>::operator<<(T value)
{

	if (value == v0)
	{

		if (!this->treep->leftChild())
		{

			typename BinRandTree<T>::Node *node = new typename BinRandTree<T>::Node(value);
			this->treep->leftChild(node);
			this->treep = this->root;
		}
		else
		{

			this->treep = this->treep->leftChild();
		}
	}
	else
	{

		if (!this->treep->rightChild())
		{

			typename BinRandTree<T>::Node *node = new typename BinRandTree<T>::Node(value);
			this->treep->rightChild(node);
			this->treep = this->root;
		}
		else
		{

			this->treep = this->treep->rightChild();
		}
	}

	return *this;
}